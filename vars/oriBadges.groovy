def addTagBadge(Map x)
{
    if (x.text == null) return

    style = "border: solid 0px; border-radius: 6px; padding: 0px, 5px; margin: 0px 0px; vertical-align: middle"    
    if (x.textColor != null) style += "; border-color: ${x.textColor}; color: ${x.textColor}"
    if (x.backgroundColor != null) style += "; background: ${x.backgroundColor}"

    addHtmlBadge html: "<span style=\"${style}\">${x.text}</span>"
}

def addBuildConfigurationBadges(Map x)
{
    if (x.buildConfiguration == null) return

    switch (x.buildConfiguration)
    {
        case "release":
            if (x.release.showBadge) addTagBadge text: "Release", textColor: "#FF0000", backgroundColor: "#FFDDDD"
            if (x.release.showBranch) addTagBadge text: x.branch
            break

        case "staging":
            if (x.staging.showBadge) addTagBadge text: "Staging", textColor: "#FF6E00", backgroundColor: "#FFEBDD"
            if (x.staging.showBranch) addTagBadge text: x.branch
            break

        case "development":
            if (x.development.showBadge) addTagBadge text: "Dev", textColor: "#808080", backgroundColor: "#E0E0E0"
            if (x.development.showBranch) addTagBadge text: x.branch
            break
    }
}
