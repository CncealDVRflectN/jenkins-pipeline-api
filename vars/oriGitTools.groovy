
def cloneOrCheckout(Map p)
{
    if (p.url == null)
    {
        error("Missing parameter url")
        return
    }

    if (p.branch == null)
    {
        error("Missing parameter branch")
        return
    }

    //if (p.credentialsId == null)
    //{
    //    error("Missing parameter credentialsId")
    //    return
    //}

    def extensions = [
        [$class: 'CleanBeforeCheckout', deleteUntrackedNestedRepositories: true],
        [$class: 'CleanCheckout'],
        [$class: 'SubmoduleOption',
            disableSubmodules: false,
            parentCredentials: true,
            recursiveSubmodules: true,
            trackingSubmodules: false]
    ]

    if (p.timeout)
    {
        extensions.add([$class: 'CloneOption', timeout: p.timeout])
        extensions.add([$class: 'CheckoutOption', timeout: p.timeout])
    }

    checkout([
            $class: 'GitSCM',
            branches: [[name: p.branch]],
            doGenerateSubmoduleConfigurations: false,
            extensions: extensions,
            submoduleCfg: [],
            userRemoteConfigs: [[/*credentialsId: p.credentialsId,*/ url: p.url]]])
}

def gitCommitId()
{
    return sh(returnStdout: true, script: 'git rev-parse HEAD')
}

def gitBranchName()
{
    return sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD')
}
