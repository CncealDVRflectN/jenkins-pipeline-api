package io.orion.jenkins.unity

import io.orion.jenkins.ShellCommandBuilder
import io.orion.jenkins.unity.UnityWrapper

// OBSOLETE
public class UnityGenericCommand extends UnityCommand
{
    UnityGenericCommand(UnityWrapper unity)
    {
        super(unity)
    }

    public void execute()
    {
        ShellCommandBuilder cmd = createCommandBuilder()
        _unity.getJenkinsBridge().sh(cmd.build())
    }
}
