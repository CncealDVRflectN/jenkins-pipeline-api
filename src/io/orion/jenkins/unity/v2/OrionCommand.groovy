package io.orion.jenkins.unity.v2

import io.orion.jenkins.ShellCommandBuilder

public class OrionCommand extends UnityCommand
{
    OrionCommand(UnityWrapper unity)
    {
        super(unity, "Orion.Cli.Entrypoint.Default")
    }
}
