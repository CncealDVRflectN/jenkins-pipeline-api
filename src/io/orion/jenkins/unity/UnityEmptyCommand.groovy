package io.orion.jenkins.unity

import io.orion.jenkins.ShellCommandBuilder
import io.orion.jenkins.unity.UnityWrapper

// OBSOLETE
public class UnityEmptyCommand extends UnityCommand
{
    UnityEmptyCommand(UnityWrapper unity)
    {
        super(unity)
    }

    public void execute()
    {
        def jenkins = _unity.getJenkinsBridge()
        setExecuteMethod("Orion.Cli.CommandLine.Execute")
        
        ShellCommandBuilder cmd = createCommandBuilder()
        jenkins.sh(cmd.build())
    }
}
