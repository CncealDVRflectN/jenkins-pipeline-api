package io.orion.jenkins.unityhub

import io.orion.jenkins.ShellCommandBuilder
import io.orion.jenkins.unity.versioning.UnityVersion

public class UnityHubEditorsCommand implements Serializable
{
    protected UnityHubWrapper _wrapper
    protected Map _args = [:]

    UnityHubEditorsCommand(UnityHubWrapper wrapper)
    {
        _wrapper = wrapper
    }

    public def setReleases(boolean value)
    {
        if (value) _args["--releases"] = null
        else _args.remove("--releases")        
        return this
    }

    public def setInstalled(boolean value)
    {
        if (value) _args["--installed"] = null
        else _args.remove("--installed")        
        return this
    }

    public Map execute()
    {
        ShellCommandBuilder cmd = _wrapper.createCommandBuilder()
            .addOption("editors")
            .addOptions(_args)

        def jenkins = _wrapper.getJenkinsBridge()
        Map result = [:]
        jenkins.echo("Searching for Unity Editor...")
        String stdout = jenkins.sh(true, cmd.build())
        if (stdout != null)
        {
            def pattern = ~/^(?<version>\d+.\d+.[^ ]+).*(?i)installed at\s(?<path>.*)$/
            def out = stdout.split("\n")
            out.each { String line ->
                def matcher = line =~ pattern
                assert matcher.matches()
                
                def version = new UnityVersion(matcher.group("version"))
                def path = matcher.group("path")

                // def pair = line.split(", installed at")
                // if (pair.size() != 2)
                // {
                //     jenkins.error("Failed to parse string '${line}'")
                //     return result
                // }

                // def version = pair[0].trim()
                // def path = pair[1].trim()
                jenkins.echo("  ${version} -> ${path}")
                result[version] = path
            }
        }

        return result
    }
}
