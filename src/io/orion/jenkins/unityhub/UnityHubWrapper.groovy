package io.orion.jenkins.unityhub

import io.orion.jenkins.IJenkinsBridge
import io.orion.jenkins.ShellCommandBuilder
import io.orion.jenkins.unity.versioning.UnityVersion
import io.orion.jenkins.unity.versioning.resolvers.IUnityVersionResolver
import io.orion.jenkins.unity.versioning.resolvers.ChainedUnityVersionResolver
import io.orion.jenkins.unity.versioning.resolvers.ExactUnityVersionResolver
import io.orion.jenkins.unity.versioning.resolvers.LatestMajorUnityVersionResolver
import com.cloudbees.groovy.cps.NonCPS

public class UnityHubWrapper implements Serializable
{
    private IJenkinsBridge _jenkins
    private String _path

    public UnityHubWrapper(IJenkinsBridge jenkins, String path)
    {
        _jenkins = jenkins
        _path = path
    }

    IJenkinsBridge getJenkinsBridge()
    {
        return _jenkins
    }

    ShellCommandBuilder createCommandBuilder()
    {
        return new ShellCommandBuilder()
            .setCommand(_path)
            .addOption("--")
            .addOption("--headless")
    }

    public UnityHubEditorsCommand getUnityHubEditorsCommand()
    {
        return new UnityHubEditorsCommand(this)
    }

    public def getUnityWrapper(Map options)
    {
        if (options.wrapperVersion == null)
            options.wrapperVersion = 1 // for keep support for old pipelines

        if (options.version != null && !options.version.isEmpty())
        {
            _jenkins.echo("Resolving Unity Editor by specified version '${options.version}'...")
            return getUnityWrapperForVersion(options.version, options.wrapperVersion)
        }

        if (options.projectPath != null)
        {
            _jenkins.echo("Resolving Unity Editor by project path '${options.projectPath}'...")
            return getUnityWrapperForProject(options.projectPath, options.wrapperVersion)
        }

        _jenkins.error("Failed to get UnityWrapper. Required at least one option: version, projectPath.")
        return null
    }

    public def getUnityWrapperForVersion(
        String versionPattern, 
        int wrapperVersion, 
        IUnityVersionResolver versionResolver = null)
    {
        Map versions = new UnityHubEditorsCommand(this)
            .setInstalled(true)
            .execute()

        if (versionResolver == null)
            versionResolver = new ExactUnityVersionResolver()

        UnityVersion version = versionResolver.resolve(
            new UnityVersion(versionPattern), versions.keySet());

        if (version == null)
        {
            _jenkins.error("Unity ${versionPattern} is not installed.")
            return null;
        }
        
        _jenkins.echo("Selected Unity ${version}")
        String path = versions[version];
        if (path == null)
        {
            _jenkins.error("Failed to get path to Unity ${version}.")
            return null;
        }

        if (wrapperVersion == 1) return new io.orion.jenkins.unity.UnityWrapper(_jenkins, path)
        if (wrapperVersion == 2) return new io.orion.jenkins.unity.v2.UnityWrapper(_jenkins, path)
        _jenkins.error("UnityWrapper version '${wrapperVersion}' not supported.")
    }

    public def getUnityWrapperForProject(
        String projectPath, 
        int wrapperVersion, 
        IUnityVersionResolver versionResolver = null)
    {
        def versionFile = "${projectPath}/ProjectSettings/ProjectVersion.txt"
        if (!_jenkins.fileExists(versionFile))
        {
            _jenkins.error("Failed to get Unity version from project at path '${projectPath}'")
            return null;
        }

        def data = _jenkins.readFile(versionFile)
        def version = nonCpsMatchString(data, /\d+\.\d+\.\d+[a-z]\d+/)
        if (version == null)
        {
            _jenkins.error("Failed to get Unity version from file '${versionFile}'")
            return null;
        }

        if (versionResolver == null)
        {
            versionResolver = new ChainedUnityVersionResolver(
                new ExactUnityVersionResolver(), 
                new LatestMajorUnityVersionResolver())
        }

        return getUnityWrapperForVersion(version, wrapperVersion, versionResolver)
    }

    @NonCPS
    private String nonCpsMatchString(data, pattern) 
    {
        def matcher = (data =~ pattern)
        if (matcher.find())
            return matcher[0]
            
        return null;
    }
}
